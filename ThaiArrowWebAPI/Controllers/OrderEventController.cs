﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ThaiArrowWebAPI.Models;

namespace ThaiArrowWebAPI.Controllers
{
    public class OrderEventController : ApiController
    {
        private THAIARROW_PART_STOREEntities db = new THAIARROW_PART_STOREEntities();
        // GET: api/OrderEvent
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/OrderEvent/5
        public IHttpActionResult Get(string UserID)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            string sql = "SELECT [order_user],[part_no],[mcline],SUM([quantity]) AS quantity FROM [THAIARROW_PART_STORE].[dbo].[Order_event] WHERE [order_user] = '" + UserID + "' AND [order_status] IS NULL GROUP BY [part_no], [order_user], [mcline]";
            List<Order_event_response> OrderList = db.Database.SqlQuery<Order_event_response>(sql).ToList();

            return Ok(OrderList);
        }

        // POST: api/OrderEvent
        public IHttpActionResult Post(Order_event order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //check order
            if (order.order_user == null || order.part_no == null || order.mcline == null) {
                return BadRequest("กรุณากรอกข้อมูลให้ครบถ้วน");
            }

            //get stock from master
            Master_part_data PartData = db.Master_part_data.Where(x => x.part_no == order.part_no).FirstOrDefault();
            if (PartData == null) {
                return BadRequest("ไม่พบข้อมูลชิ้นส่วนที่ต้องการ");
            }
            int stockCount = PartData.stock_available ?? 0;

            //get order for reserve data
            List<Order_event> OrderList = db.Order_event.Where(x => x.part_no == order.part_no && x.order_status == null).ToList();
            int reserveCount = 0;
            for (int i=0;i<OrderList.Count;i++) {
                reserveCount += OrderList[i].quantity;
            }
            int StockAvailable = stockCount - reserveCount;
            if (order.quantity < 1)
            {
                order.quantity = 1;
            }
            if (StockAvailable < order.quantity) {
                return BadRequest("จำนวนที่ต้องการเบิกมากกว่าจำนวนที่เบิกได้");
            }

            order.order_time = DateTime.Now;

            int qty = order.quantity;
            order.quantity = 1;
            //insert
            for (int i = 0;i<qty;i++) {
                db.Order_event.Add(order);
                db.SaveChanges();
            }
            //db.Order_event.Add(order);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = order.order_id }, order);
        }

        // PUT: api/OrderEvent/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/OrderEvent/5
        public void Delete(int id)
        {
        }
    }

    class Order_event_response
    {
        public string order_user { get; set; }
        public string part_no { get; set; }
        public string mcline { get; set; }
        public int quantity { get; set; }
    }
}
