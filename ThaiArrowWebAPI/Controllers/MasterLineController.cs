﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ThaiArrowWebAPI.Models;

namespace ThaiArrowWebAPI.Controllers
{
    public class MasterLineController : ApiController
    {
        private THAIARROW_PART_STOREEntities db = new THAIARROW_PART_STOREEntities();

        // GET: api/MasterLine
        public IQueryable<Master_line> GetMasterLine()
        {
            return db.Master_line;
        }

        // GET: api/MasterLine/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/MasterLine
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/MasterLine/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/MasterLine/5
        public void Delete(int id)
        {
        }
    }
}
