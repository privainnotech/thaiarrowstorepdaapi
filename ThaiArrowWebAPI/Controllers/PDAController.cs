﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ThaiArrowWebAPI.Models;

namespace ThaiArrowWebAPI.Controllers
{
    public class PDAController : ApiController
    {
        private THAIARROW_PART_STOREEntities db = new THAIARROW_PART_STOREEntities();

        //// GET: api/PDA
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/PDA/5
        [Route("api/PDA/CodeData")]
        public IHttpActionResult GetCodeData(string BoxID, string PartNo)
        {
            CodeDataResponse response = new CodeDataResponse();
            response.StockPart = db.Stock_part.Where(x => x.box_id == BoxID).FirstOrDefault();
            response.PartData = db.Master_part_data.Where(x => x.part_no == PartNo).FirstOrDefault();
            response.OrderEventList = db.Order_event.Where(x => x.part_no == PartNo && x.order_status == null).ToList();
            List<Master_line_part> LinePart = db.Master_line_part.Where(x => x.part_no == PartNo).ToList();
            response.MCList = new List<string>();
            for (int i=0;i<LinePart.Count;i++) {
                string mc = LinePart[i].line_no;
                response.MCList.Add(mc);
            }

            return Ok(response);
        }

        // POST: api/PDA
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/PDA/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/PDA/5
        public void Delete(int id)
        {
        }
    }

    class CodeDataResponse
    {
        public Stock_part StockPart { get; set; }
        public Master_part_data PartData { get; set; }
        public List<Order_event> OrderEventList { get; set; }
        public List<string> MCList { get; set; }
    }
}
