﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using System.Web.Http.Description;
using ThaiArrowWebAPI.Models;

namespace ThaiArrowWebAPI.Controllers
{
    public class UserLoginController : ApiController
    {
        private THAIARROW_PART_STOREEntities db = new THAIARROW_PART_STOREEntities();

        //// GET: api/UserLogin
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/UserLogin/5
        public IHttpActionResult GetUserLogin(string userid, string password)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User_data user = db.User_data.Where(u => u.username == userid && u.password == password).FirstOrDefault();
            if (user == null)
            {
                return NotFound();
            }

            List<User_permission> permission = db.User_permission.Where(u => u.username == userid).ToList();
            UserLoginResponse response = new UserLoginResponse();
            response.userData = user;
            response.UserPermission = permission;
            return Ok(response);
        }

        // POST: api/UserLogin
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/UserLogin/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/UserLogin/5
        public void Delete(int id)
        {
        }
    }

    class UserLoginResponse {
        public User_data userData { get; set; }
        public List<User_permission> UserPermission { get; set; }
    }
}
